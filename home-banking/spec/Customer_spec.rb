require 'rspec'
require_relative '../model/Customer'

describe 'Customer' do

	it 'create a customer with user nicolas@gmail.com and password nico1234' do
		user = "nicolas@gmail.com"
		password = "nico1234"
		customer = Customer.new(user, password)
		expect(customer.user).to eq user
		expect(customer.password).to eq password
	end

end