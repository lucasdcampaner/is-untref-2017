require 'rspec'
require_relative '../model/ATM'

describe 'ATM' do

    it 'generate code registration' do

        code_expect = "123456"
        
        atm = ATM.new()
        code_actual = atm.generate_code(code_expect)

        expect(code_expect).to eq code_actual
    end

end