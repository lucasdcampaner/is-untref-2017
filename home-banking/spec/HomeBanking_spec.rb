require 'rspec'
require_relative '../model/HomeBanking'

describe 'HomeBanking' do

    it 'create a home banking with one user' do

        user = "nicolas@gmail.com"
        password = "nico1234"
        
        home_banking = HomeBanking.new()
        home_banking.add_user(user, password)

        expect(home_banking.get_password_by_user(user)).to eq password
    end
end