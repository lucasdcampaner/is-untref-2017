class ATM

    attr_accessor :home_banking

    @user_codes
    @password_size

    def initialize()
        @user_codes = Hash.new
        @home_banking = HomeBanking.new
        @password_size = 8
    end

    def generate_code(code)
        @user_codes["jd@free.com"] = code
        code
    end

    def register(email, password, code)
        account_created = false
        if @user_codes[email] == code && valid_password(password)
            @home_banking.add_user(email, password)
            account_created = true
        end
        account_created
    end

    def valid_password(password)
        password.size >= @password_size && (password =~ /[A-Z]/).eql?(0)
    end
end