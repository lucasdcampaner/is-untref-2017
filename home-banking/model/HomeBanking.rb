class HomeBanking

    attr_accessor :users, :attemp_login_failure

    @users
    @attemp_login_failure

    def initialize()
        @users = Hash.new
        @attemp_login_failure = Hash.new
    end
    
    def add_user(user, password)
        @users[user] = password
        @attemp_login_failure[user] = 0
    end

    def get_password_by_user(user)
        @users[user]
    end

    def login(user, password)
        if @users[user] != password
            if @attemp_login_failure[user] != nil
                @attemp_login_failure[user] = @attemp_login_failure[user] + 1
            end
            false
        else
            @attemp_login_failure[user] = 0
            true
        end
    end

    def credentials_blocked?(user)
        @attemp_login_failure[user] > 2
    end

end