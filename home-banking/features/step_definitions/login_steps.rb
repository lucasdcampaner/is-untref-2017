Given(/^my user is "([^"]*)" and password "([^"]*)"$/) do |user, password|
    @home_banking = HomeBanking.new()
    @home_banking.add_user(user, password)
end

When(/^I login with "([^"]*)" and password "([^"]*)"$/) do |user, password|
    @login_succes = @home_banking.login(user, password)
end

Then(/^I access the system$/) do
    expect(@login_succes).to eq true
end

Then(/^I do not access the system$/) do
    expect(@login_succes).to eq false
end