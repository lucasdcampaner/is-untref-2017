Given(/^the ATM generated code "([^"]*)"$/) do |code|
    @atm = ATM.new()
    @atm.generate_code(code)
end

When(/^I register with email "([^"]*)", password "([^"]*)" and code "([^"]*)"$/) do |email, password, code|
    @account_created = @atm.register(email, password, code)
end

Then(/^my account is created$/) do
    expect(@account_created).to eq true
end

Then(/^my account is not created$/) do
    expect(@account_created).to eq false
end
