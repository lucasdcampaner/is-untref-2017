Given(/^I have already registered$/) do
    @code = "123456"
    @atm = ATM.new()
    @atm.generate_code(@code)
    @user = "jd@free.com"
    @password = "Passw0rd!"
    @atm.register(@user, @password, @code)
end

When(/^I attemp (\d+) failed logins$/) do |attemps|
    @password_wrong = "AAssw0rd!"
    attemp = 0
    while attemp < attemps.to_i  do
        @atm.home_banking.login(@user, @password_wrong)    
        attemp +=1
    end    
end

Then(/^my credentials get blocked$/) do
    expect(@atm.home_banking.credentials_blocked?(@user)).to eq true
end
