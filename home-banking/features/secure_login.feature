Feature: Secure Login
    As a customer
    I want to login to be secure

Scenario: Blocked after 3 failed login attemps
    Given I have already registered 
    When I attemp 3 failed logins
    Then my credentials get blocked
